using System;
using System.Linq;
using System.Text;

// visit type checked tree and generate code
namespace AntlrTest
{
public class CodeGenerator
{
	private int tmpCounter = 0;

	public CodeGenerator()
	{

	}

	private string CreateTemp()
	{
		return "tmp" + tmpCounter++.ToString();
	}

	public void eval_bool(AstNode node, string true_branch, string false_branch, StringBuilder builder)
	{
		if (node is BooleanExpressionAstNode)
		{
			var bnode = node as BooleanExpressionAstNode;
			builder.Append(string.Format("if ({0} == lit_true) goto {1};\r\n", Gen(bnode, builder), true_branch));
			builder.Append(string.Format("goto {0};\r\n", false_branch));
		}
		else if (node is LogicExpressionAstNode)
		{
			var mnode = node as LogicExpressionAstNode;
			if (mnode.Name == "AND")
			{
				// Console.WriteLine("{0} and {1}", mnode.Left, mnode.Right);
				var left = mnode.Left;
				var right = mnode.Right;
				var halfway = string.Format("and{0}", CreateTemp());
				eval_bool(left, halfway, false_branch, builder);
				builder.Append(string.Format("{0}:;\r\n", halfway));
				eval_bool(right, true_branch, false_branch, builder);
			}
			else if (mnode.Name == "OR")
			{
				// Console.WriteLine("{0} or {1}", mnode.Left, mnode.Right);
				var left = mnode.Left;
				var right = mnode.Right;
				var halfway = string.Format("or{0}", CreateTemp());
				eval_bool(left, true_branch, halfway, builder);
				builder.Append(string.Format("{0}:;\r\n", halfway));
				eval_bool(right, true_branch, false_branch, builder);
			}
			else if (mnode.Name == "NOT")
			{
				// Console.WriteLine("not {0}", mnode.Right);
				builder.Append(string.Format("if ({0} == lit_true) goto {1};\r\n", Gen(mnode.Right, builder), false_branch));
				builder.Append(string.Format("goto {0};\r\n", true_branch));
			}
			else
			{
				Program.ReportError(node.Context.Start, string.Format("Invalid LogicExpression encountered: '{0}'", mnode.Name));
			}
		}
		else if (node is MethodCallNode)
		{
			var mnode = node as MethodCallNode;
			builder.Append(string.Format("if ({0} == lit_true) goto {1};\r\n", Gen(mnode, builder), true_branch));
			builder.Append(string.Format("goto {0};\r\n", false_branch));							
		}
	}

	public string Gen(StatementAstNode node, StringBuilder builder, bool constructor = false)
	{
		var tmp = CreateTemp();

		if (node is AssignStatementAstNode)
		{
			var anode = node as AssignStatementAstNode;
			var left = Gen(anode.Left, builder, constructor);
			var right = Gen(anode.Right, builder, constructor);
			builder.Append(string.Format("// {0}:{1} = {2}:{3}\r\n", left, anode.Left.Type, right, anode.Right.Type));
			//var str = ("  " + Gen(anode.Left, builder) + " = " + Gen(anode.Right, builder) + ";\r\n");
			builder.Append(string.Format("  {0} = (obj_{1}) {2};\r\n",
				left, anode.Left.Type, right));
			tmp = "";
		}
		else if (node is BooleanExpressionAstNode)
		{
			var bnode = node as BooleanExpressionAstNode;
			if (bnode.Value == true)
			{
				builder.Append(string.Format("  obj_Boolean {0} = lit_true;\r\n", tmp));
			}
			else 
			{
				builder.Append(string.Format("  obj_Boolean {0} = lit_false;\r\n", tmp));
			}
		}
		else if (node is IdentifierExpressionAstNode)
		{
			var inode = node as IdentifierExpressionAstNode;
			builder.Append(string.Format("  obj_{0} {1} = (obj_{0}) {2};\r\n", inode.Type, tmp, inode.Name));
		}
		else if (node is IfStatementAstNode)
		{
			var inode = node as IfStatementAstNode;
			var iftmp = tmp;
			foreach (var branch in inode.Branches)
			{
				eval_bool(branch.Condition, string.Format("true_branch{0}", tmp), string.Format("false_branch{0}", tmp), builder);			
				builder.Append(string.Format("true_branch{0}:;\r\n", tmp));
				foreach (var s in branch.Statements)
				{
					Gen(s, builder, constructor);
				}
				builder.Append(string.Format("goto end_block{0};\r\n", iftmp));
				builder.Append(string.Format("false_branch{0}:;\r\n", tmp));
				tmp = CreateTemp();
			}
			builder.Append(string.Format("end_block{0}:;\r\n", iftmp));

			tmp = "";
		}
		else if (node is DotIdentifierExpressionAstNode)
		{
			var inode = node as DotIdentifierExpressionAstNode;
			if (constructor && inode.Expression.Name == "this")
			{
				builder.Append(string.Format("  obj_{0} {1} = new_thing->{2}; // constructor??\r\n", inode.Type, tmp, inode.Name));
			}
			else
			{
				builder.Append(string.Format("  obj_{0} {1} = {2}->{3};\r\n", inode.Type, tmp, inode.Expression.Name, inode.Name));			
			}
		}
		else if (node is IntegerExpressionAstNode)
		{
			var inode = node as IntegerExpressionAstNode;

			return string.Format("int_literal({0})", inode.Value.ToString());
		}
		else if (node is LeftExpressionAstNode)
		{
			var lnode = node as LeftExpressionAstNode;
			var str = string.Empty;
			if (lnode.Expression != null)
			{
				if (lnode.Expression is IdentifierExpressionAstNode)
				{
					if (constructor)
					{
						str = "new_thing->";						
					}
					else
					{
						str = string.Format("{0}->", lnode.Expression.Name);
					}
				}
				else
				{
					str = Gen(lnode.Expression, builder, constructor);
				}
			}
			str += lnode.Identifier.Text;
			return str;
		}
		else if (node is MethodCallNode)
		{
			var mnode = node as MethodCallNode;
			var left = "";
			var str = string.Empty;

			if (mnode.Expression != null)
			{				
				left = Gen(mnode.Expression, builder, constructor);
				if (mnode.Method != null)
				{
					str += string.Format("{0}->clazz->{1}((obj_{2}) {0}", left, mnode.Name, mnode.Method.OwnerClass);
					for (var i = 0; i < mnode.ActualArgs.Count; i++)
					{
						str += string.Format(", (obj_{0}) {1}", mnode.Method.Args[i].Type, Gen(mnode.ActualArgs[i], builder, constructor));
					}
				}
				else
				{
					Console.WriteLine("method call {0} has no method property", mnode.Name);
				}
			}
			else
			{
				str += string.Format("the_class_{0}->constructor(", mnode.Type);
				for (var i = 0; i < mnode.ActualArgs.Count; i++)
				{
					if (i > 0)
					{
						str += ", ";
					}
					str += string.Format("(obj_{0}) {1}", mnode.ActualArgs[i].Type, Gen(mnode.ActualArgs[i], builder, constructor));
				}
			}
			str += ")";
			builder.Append(string.Format("  obj_{0} {1} = (obj_{0}) {2};\r\n", mnode.Type, tmp, str));
			//tmp = "";
		}
		else if (node is ReturnStatementAstNode)
		{
			var rnode = node as ReturnStatementAstNode;
			var str = "  return";
			if (rnode.Expression != null)
			{
				str += string.Format(" {0}", Gen(rnode.Expression, builder));
			}
			builder.Append(str + ";\r\n");
		}
		else if (node is StringExpressionAstNode)
		{
			var snode = node as StringExpressionAstNode;

			// tmp = "";
			builder.Append(string.Format("  obj_String {0} = str_literal(\"{1}\");\r\n", tmp, snode.Text));
		}
		else if (node is WhileStatementAstNode)
		{
			var wnode = node as WhileStatementAstNode;

			builder.Append(string.Format("goto cond{0};\r\n", tmp));
			builder.Append(string.Format("block{0}:;\r\n", tmp));
			foreach (var s in wnode.Statements)
			{
				Gen(s, builder);// + ";\r\n";				
			}
			builder.Append(string.Format("cond{0}:;\r\n", tmp));
			eval_bool(wnode.Condition, string.Format("block{0}", tmp), string.Format("loop_exit{0}", tmp), builder);
			// str += "  eval_bool(" + Gen(wnode.Condition, builder) + ", block, loop_exit);\r\n";
			builder.Append(string.Format("loop_exit{0}:;\r\n", tmp));
		}		
		else
		{
			Console.WriteLine("handle {0}", node);
		}

		return tmp;
	}

	public void Generate(ProgramAstNode checkedAst)
	{
		var builder = new StringBuilder();
		// add headers
		builder.Append("#define _GNU_SOURCE\r\n");
		builder.Append("#include <stdio.h>\r\n");
		builder.Append("#include <stdlib.h>\r\n");
		builder.Append("#include <string.h>\r\n");
		builder.Append("#include \"Builtins.h\"\r\n\r\n");
		// boilerplate
		builder.Append("void quackmain();\r\n");
		builder.Append("int main(int argc, char** argv) {\r\n");
      	builder.Append("  quackmain();\r\n");
      	builder.Append("  exit(0);\r\n");
      	builder.Append("}\r\n");
      	
		foreach (var c in checkedAst.Classes)
		{
			if (new string[] {"Obj", "Int", "String", "Boolean", "Nothing", "_MAIN_"}.Contains(c.Value.Name)) continue;

			builder.Append(string.Format("struct class_{0}_struct;\r\n", c.Value.Name));
			builder.Append(string.Format("typedef struct class_{0}_struct* class_{0};\r\n", c.Value.Name));
			builder.Append(string.Format("typedef struct obj_{0}_struct {{\r\n", c.Value.Name));
			builder.Append(string.Format("  class_{0} clazz; // should have {1} fields\r\n", c.Value.Name, c.Value.Fields.Count));
			foreach (var f in c.Value.Fields)
			{
				builder.Append(string.Format("  obj_{0} {1};\r\n", f.Type, f.Name));
			}
			builder.Append(string.Format("}} * obj_{0};\r\n", c.Value.Name));

			builder.Append(string.Format("struct class_{0}_struct {{\r\n", c.Value.Name));
			for (var i = 0; i < c.Value.Methods.Count; i++)
			{
				if (c.Value.Methods[i].Name == c.Value.Name)
				{
	  				builder.Append(string.Format("  obj_{0} (*constructor) (", c.Value.Name));					
	  				// constructor args
					for (var a = 0; a < c.Value.Methods[i].Args.Count; a++)
					{
						if (a != 0)
						{
							builder.Append(", ");
						}
						builder.Append(string.Format("obj_{0}", c.Value.Methods[i].Args[a].Type));
					}
				}
				else
				{
					builder.Append(string.Format("  obj_{0} (*{1}) (", c.Value.Methods[i].Type, c.Value.Methods[i].Name));
					builder.Append(string.Format("obj_{0}", c.Value.Methods[i].OwnerClass));
					// method args
					for (var a = 0; a < c.Value.Methods[i].Args.Count; a++)
					{
						builder.Append(string.Format(", obj_{0}", c.Value.Methods[i].Args[a].Type));
					}
				}

				builder.Append(");\r\n");
			}
			builder.Append("};\r\n");

			builder.Append(string.Format("extern class_{0} the_class_{0};\r\n", c.Value.Name));
			foreach (var m in c.Value.DeclaredMethods)
			{
				if (m.Name == c.Value.Name)
				{
					// constructor
        			builder.Append("/* Constructor */\r\n");
					builder.Append(string.Format("obj_{0} new_{0} (", c.Value.Name));
					for (var a = 0; a < m.Args.Count; a++)
					{
						if (a != 0)
						{
							builder.Append(", ");
						}
						builder.Append(string.Format("obj_{0} {1}", m.Args[a].Type, m.Args[a].Name));
					}
					builder.Append(") {\r\n");
      				builder.Append(string.Format("  obj_{0} new_thing = (obj_{0}) malloc(sizeof(struct obj_{0}_struct));\r\n", c.Value.Name));
      				builder.Append(string.Format("  new_thing->clazz = the_class_{0};\r\n", c.Value.Name));
      				// loop through statements and generate code
					foreach (var v in m.Variables)
					{
						builder.Append(string.Format("  obj_{0} {1};\r\n", v.Type, v.Name));
					}
					foreach (var s in m.Statements)
					{
						//builder.Append(Gen(s, builder, true));
						Gen(s, builder, true);
					}

					builder.Append("  return new_thing;\r\n");
					builder.Append("}\r\n");
				}
				else
				{
					// method
					builder.Append(string.Format("/* {0} method */\r\n", m.Name));
					builder.Append(string.Format("obj_{0} {2}_method_{1}(obj_{2} this", m.Type, m.Name, c.Value.Name));
					foreach (var a in m.Args)
					{
						builder.Append(string.Format(", obj_{0} {1}", a.Type, a.Name));
					}
					builder.Append(") {\r\n");
					foreach (var v in m.Variables)
					{
						builder.Append(string.Format("  obj_{0} {1};\r\n", v.Type, v.Name));
					}
					foreach (var s in m.Statements)
					{
						//builder.Append(Gen(s, builder));
						Gen(s, builder);
					}
					builder.Append("};\r\n");
				}
			}

			// class definition
			builder.Append(string.Format("// the {0} class (a singleton)\r\n", c.Value.Name));
			builder.Append(string.Format("struct class_{0}_struct the_class_{0}_struct = {{\r\n", c.Value.Name));
			for (var i = 0; i < c.Value.Methods.Count; i++)
			{
				if (c.Value.Methods[i].Name == c.Value.Name)
				{
	  				builder.Append(string.Format("  new_{0}", c.Value.Name));					
				}
				else
				{
					builder.Append(string.Format("  {0}_method_{1}", c.Value.Methods[i].OwnerClass, c.Value.Methods[i].Name));
				}

				if (i < c.Value.Methods.Count-1)
				{
					builder.Append(",");
				}
				builder.Append("\r\n");
			}

			builder.Append("};\r\n");

			builder.Append(string.Format("class_{0} the_class_{0} = &the_class_{0}_struct;\r\n", c.Value.Name));
		}

		// generate quackmain()
		var main = checkedAst.Classes["_MAIN_"];
		builder.Append("void quackmain() {\r\n");
		// builder.Append("  // do some stuff\r\n");
		// builder.Append(string.Format("  // {0} has {1} statements\r\n", main.Name, main.DeclaredMethods[0].Statements.Count));
		foreach (var v in main.Methods[0].Variables)
		{
			builder.Append(string.Format("  obj_{0} {1};\r\n", v.Type, v.Name));
		}
		foreach (var s in main.DeclaredMethods[0].Statements)
		{
			Gen(s, builder);
		}
		builder.Append("}\r\n");

		using (System.IO.StreamWriter file = 
            new System.IO.StreamWriter("generated.c", false))
        {
            file.Write(builder.ToString());
        }
		// Console.Write(builder);
	}
}
}
