using System;
using System.Collections.Generic;
using System.Linq;

namespace AntlrTest
{
public class TypeChecker
{
    private ProgramAstNode program;
    private bool _changed = false;
    // private bool _whileChanged = false;

    public TypeChecker()
    {

    }

    private bool SetType(AstNode source, AstNode left, AstNode right)
    {
        var success = false;
        List<LeftExpressionAstNode> variables = null;

        if (right.Type == "Bottom")
        {
            return false;
        }

        if (source is ClassAstNode)
        {
            variables = ((ClassAstNode)source).DeclaredFields;
        }
        else if (source is MethodAstNode)
        {
            variables = ((MethodAstNode)source).Variables;
        }

        if (variables != null)
        {
            foreach (var variable in variables)
            {
                if (variable.Name == left.Name)
                {
                    var type = right.Type;
                    if (variable.Type == type)
                    {
                        return true;
                    }

                    if (!string.IsNullOrEmpty(variable.DeclaredType))
                    {
                        // IsSubClassOf ( subclass , superclass )
                        Console.WriteLine("check {0} is subtype of {1}", type, variable.DeclaredType);
                        if (IsSubClassOf(type, variable.DeclaredType))
                        {
                            // leave the type alone if it is a subclass of the declared type
                            // Console.WriteLine("<not> updating {0}:{1} [{2}] to type {3}", variable.Name, variable.Type, variable.DeclaredType, type);
                            return true;
                        }
                        else
                        {
                            Program.ReportError(right.Context.Start, string.Format("Unable to update declared type '{0}' to '{1}'", variable.DeclaredType, type));
                            return false;
                        }
                    }
                    else
                    {
                        type = LeastCommonAncestor(variable.Type, right.Type);
                        // Console.WriteLine("updating {0}:{1} [{2}] to type {3}", variable.Name, variable.Type, variable.DeclaredType, type);
                    }
                    // Console.WriteLine("update {0}:{1} to {0}:{2}", variable.Name, variable.Type, type);
                    variable.Type = type;
                    _changed = true;
                    success = true;
                    break;
                }
            }
        }

        return success;
    }

    private bool CheckSymbolTable(List<LeftExpressionAstNode> symbols, StatementAstNode ident)
    {
        var found = false;
        foreach (var s in symbols)
        {
            // Console.WriteLine("check {0} == {1}?", s.Name, ident.Name);
            if (s.Name == ident.Name)
            {
                // Console.WriteLine("{0}:{1} == {2}:{3}", s.Name, s.Type, ident.Name, ident.Type);
                if (string.IsNullOrEmpty(ident.Type) || ident.Type == "Bottom")
                {
                    // Console.WriteLine("update {0}:{1} to {2}", ident.Name, ident.Type, s.Type);
                    ident.Type = s.Type;                    
                }
                else if (ident.Type != s.Type)
                {
                    // Console.WriteLine("should update {2}:{3} to {0}:{1}", s.Name, s.Type, ident.Name, ident.Type);
                    ident.Type = s.Type;
                }
                found = true;
                break;
            }
        }
        return found;
    }

    private bool CheckClassExists(ProgramAstNode programNode, string className)
    {
        foreach (var c in programNode.Classes)
        {
            if (c.Key == className)
            {
                return true;
            }
        }
        // Console.WriteLine("no class {0}", className);
        return false;
    }

    public bool IsSubClassOf(string left, string right)
    {
        // Console.WriteLine("{0} <= {1}??", left, right);
        // we know the classes are valid at this point
        var retval = false;

        if (left == "Bottom" || right == "Bottom")
        {
            retval = false;
        }
        else if (left == right)
        {
            retval = true;
        }
        else
        {
            if (!(left == "Obj"))
            {
                //Console.WriteLine("left {0} parent {1}", left, ast.Classes[left].Super);
                retval = IsSubClassOf(program.Classes[left].Super, right);
            }
        }

        return retval;
    }

    // find the least common ancestor of two classes
    public string LeastCommonAncestor(string left, string right)
    {
        // Console.WriteLine("LCA {0} {1}", left, right);
        var lca = "Obj"; // default LCA is "Obj"
        var leftParents = new List<string>();
        if (left == "Bottom" || string.IsNullOrEmpty(left))
        {
            lca = right;
        }
        else if (right == "Bottom" || string.IsNullOrEmpty(right))
        {
            lca = left;
        }
        else
        {
            while (left != "Obj") {
                leftParents.Add(left);
                left = program.Classes[left].Super;
            }

            // find the first intersection with left node's parents
            while (right != "Obj") {
                if (leftParents.Contains(right)) {
                    lca = right;
                    break;
                }
                right = program.Classes[right].Super;
            }
        }

        return lca;
    }

    public static void TypeCheckArgs(List<LeftExpressionAstNode> args, Dictionary<string, ClassAstNode> classes)
    {
        foreach (var a in args)
        {
            // Console.WriteLine("check {0} for type {1}", a.Name, a.Type);
            if (!classes.ContainsKey(a.Type))
            {
                Program.ReportError(a.Context.Start, string.Format("The type '{0}' could not be found.", a.Type));
            }
        }
    }

    // return a type-checked ProgramAsNode
    public ProgramAstNode TypeCheck(ProgramAstNode programNode)
    {
        program = programNode;

        foreach (var c in programNode.Classes)
        {
            if ((new string[] {"Obj", "String", "Int", "Boolean", "Nothing"}).Contains(c.Value.Name))
            {
                continue;
            }

            Program.Debug(string.Format("checking class {0}", c.Value.Name), 1);
            foreach (var m in c.Value.DeclaredMethods)
            {
                Program.Debug(string.Format("    checking method {0} ({1})", m.Name, m.ReturnType), 1);
                TypeCheck(c.Value, m);

                if (string.IsNullOrEmpty(m.ReturnType))
                {
                    m.ReturnType = "Nothing";
                }

                if (!IsSubClassOf(m.ReturnType, m.Type))
                {
                    Program.ReportError(m.Context.Start, string.Format("[{0}] Invalid return type '{1}', expected '{2}'", m.Name, m.ReturnType, m.Type));
                }
            }
        }

        return programNode;
    }

    public MethodAstNode TypeCheck(ClassAstNode classNode, MethodAstNode methodNode)
    {
        TypeCheckArgs(methodNode.Args, program.Classes);

        // Console.WriteLine("changed: {0}", _changed);
        for (var i = 0; i < 10 && Program.OtherErrors == 0; i++)
        {
            _changed = false;
            foreach (var stmt in methodNode.Statements)
            {
                Program.Debug(string.Format("        check method statement: {0}:{1}", stmt, stmt.Type), 1);
                TypeCheck(classNode, methodNode, stmt);
            }
            if (!_changed)
            {
                break;
            }
            // Console.WriteLine("changed: {0}", _changed);            
        }

        return methodNode;
    }

    public StatementAstNode TypeCheck(ClassAstNode classNode, MethodAstNode methodNode, StatementAstNode stmt)
    {
        if (stmt is AssignStatementAstNode)
        {
            var assign_stmt = stmt as AssignStatementAstNode;
            var right = TypeCheck(classNode, methodNode, assign_stmt.Right);
            var left = (LeftExpressionAstNode)TypeCheck(classNode, methodNode, assign_stmt.Left);

            // if (!string.IsNullOrEmpty(assign_stmt.Type))
            // {
            //     left.Type = assign_stmt.Type;
            //     //left.DeclaredType = assign_stmt.Type;
            // }
            // Console.WriteLine("({0}) {1}:{2} = {3}:{4}", methodNode.Name, left, left.Type, right, right.Type);
            if (left.Type == "Bottom" && right is IdentifierExpressionAstNode)
            {
                if (CheckSymbolTable(methodNode.Args, right)) {}
                else if (CheckSymbolTable(methodNode.Variables, right)) 
                {
                    // Console.WriteLine("found variable {0}:{1} [statement]", right.Name, right.Type);
                }
                else
                {
                    Program.ReportError(right.Context.Start, string.Format("Use of unassigned local variable '{0}'", right.Name));
                }
            }

            if (left.Expression != null)
            {
                // Console.WriteLine("[c] setting {0} to {1}", assign_stmt.Left, right);
                SetType(classNode, left, right);
            }
            else
            {
                // Console.WriteLine("[m] setting {0}:{1} to {2}", assign_stmt.Left.Name, assign_stmt.Left.Type, right.Type);
                SetType(methodNode, assign_stmt.Left, right);
            }

            stmt.Type = right.Type;
        }
        else if (stmt is IntegerExpressionAstNode)
        {
            // var iexpr = stmt as IntegerExpressionAstNode;
            // Console.WriteLine("int literal {0}:{1}", iexpr.Value, iexpr.Type);
        }
        else if (stmt is BooleanExpressionAstNode)
        {
            // a bare boolean expression by itself
        }
        else if (stmt is DotIdentifierExpressionAstNode)
        {
            var dexpr = stmt as DotIdentifierExpressionAstNode;
            // type check identifier
            // Console.WriteLine("check method symbol table for {0}", dexpr.Name);
            if (dexpr.Expression != null)
            {
                // Console.WriteLine("dexpr {0}.{1}:{2}", dexpr.Expression.Name, dexpr.Name, dexpr.Expression.Type);
                if (dexpr.Expression.Name == "this")
                {
                    // Console.WriteLine("checking {0} fields for {1}", classNode.Name, dexpr.Name);
                    if (CheckSymbolTable(classNode.DeclaredFields, dexpr)) {}
                    else
                    {
                        Program.ReportError(dexpr.Context.Start, string.Format("[dotident] Method '{0}' does not contain a definition for '{1}.{2}'", methodNode.Name, dexpr.Expression.Name, dexpr.Name));
                    }

                }
                else
                {
                    var exp = TypeCheck(classNode, methodNode, dexpr.Expression);

                    // Console.WriteLine("checking '{0}' fields for {1}", dexpr.Expression.Type, dexpr.Name);
                    // Console.WriteLine("dexpr {0}.{1}: {2}:{3}", exp.Name, dexpr.Name, exp, exp.Type);
                    if (exp is MethodCallNode)
                    {
                        if (CheckClassExists(program, exp.Type))
                        {
                            if (!IsSubClassOf(classNode.Name, exp.Type))
                            {
                                Program.ReportError(dexpr.Expression.Context.Start, string.Format("Unable to access fields of class {0} from class {1}.", exp.Type, classNode.Name));
                            }
                            else
                            {
                                if (CheckSymbolTable(program.Classes[exp.Type].DeclaredFields, dexpr)) {}
                                else
                                {
                                    Console.WriteLine("class {0} doesn't have field {1}", exp.Type, dexpr.Name);
                                }                            
                            }                            
                        }
                    }
                    else if (CheckSymbolTable(classNode.DeclaredFields, dexpr))
                    {
                        // Console.WriteLine("found field {0}", dexpr.Name);
                    }
                    else if (CheckSymbolTable(methodNode.Variables, dexpr.Expression)) 
                    {
                        // Console.WriteLine("found {0}:{1} in symbol table", dexpr.Expression.Name, dexpr.Expression.Type);
                        if (CheckSymbolTable(program.Classes[dexpr.Expression.Type].DeclaredFields, dexpr))
                        {
                            // Console.WriteLine("found {0}.{1}:{2}", dexpr.Expression.Type, dexpr.Name, dexpr.Type);
                        }
                        else
                        {
                            Console.WriteLine("{0} has {1} fields but no {2}", dexpr.Expression.Type, program.Classes[dexpr.Expression.Type].DeclaredFields.Count, dexpr.Name);
                        }
                    }
                    else
                    {
                        Program.ReportError(dexpr.Context.Start, string.Format("[dotident2] Method '{0}' does not contain a definition for '{1}.{2}'", methodNode.Name, dexpr.Expression.Name, dexpr.Name));
                    }
                }
            }
            else if (CheckSymbolTable(methodNode.Args, dexpr)) {}
            else if (CheckSymbolTable(methodNode.Variables, dexpr))
            {
                // Console.WriteLine("found variable {0}:{1} [dotident]", dexpr.Name, dexpr.Type);
            }
            else
            {
                Program.ReportError(dexpr.Context.Start, string.Format("[dotident] Method '{0}' does not contain a definition for '{1}'", methodNode.Name, dexpr.Name));
            }
        }
        else if (stmt is IdentifierExpressionAstNode)
        {
            var ident = stmt as IdentifierExpressionAstNode;
            // type check identifier
            // Console.WriteLine("check method symbol table ({0} args) for {1}", methodNode.Args.Count, ident.Name);
            if (CheckSymbolTable(methodNode.Args, ident)) 
            {
                // Console.WriteLine("found arg {0}", ident.Name);
            }
            else if (CheckSymbolTable(methodNode.Variables, ident)) 
            {
                // Console.WriteLine("found variable {0}:{1} [ident]", ident.Name, ident.Type);
            }
            else
            {
                Program.ReportError(ident.Context.Start, string.Format("[ident] Method '{0}' does not contain a definition for '{1}'", methodNode.Name, ident.Name));
            }

        }
        else if (stmt is IfStatementAstNode)
        {
            var ifnode = stmt as IfStatementAstNode;

            foreach (var branch in ifnode.Branches)
            {
                var cond = TypeCheck(classNode, methodNode, branch.Condition);

                // Console.WriteLine("if branch {0}:{1}", cond, cond.Type);
                if (cond.Type != "Boolean")
                {
                    Program.ReportError(cond.Context.Start, string.Format("Cannot convert type '{0}' to 'Boolean'", cond.Type));
                } 
                else 
                {
                    foreach (var s in branch.Statements)
                    {
                        if (s is ReturnStatementAstNode)
                        {
                            ifnode.HasReturn = true;
                            branch.HasReturn = true;
                        }
                        TypeCheck(classNode, methodNode, s);
                    }
                    if (ifnode.HasReturn && !branch.HasReturn)
                    {
                        Program.ReportError(ifnode.Context.Start, string.Format("Not all paths have a return value"));
                    }
                }
            }
        }
        else if (stmt is IntegerExpressionAstNode)
        {
            var inode = stmt as IntegerExpressionAstNode;

            if (inode.Type != "Int")
            {
                Program.ReportError(inode.Context.Start, string.Format("Integer literal '{0}' with invalid type '{1}'", inode.Value, inode.Type));
            }
        }
        else if (stmt is LeftExpressionAstNode)
        {
            var l_expr = stmt as LeftExpressionAstNode;
            if (l_expr.Expression != null)
            {
                if (l_expr.Expression.Name == "this")
                {
                    if (CheckSymbolTable(classNode.DeclaredFields, l_expr))
                    {
                        // Console.WriteLine("found variable {0}:{1} [expression]", l_expr.Name, l_expr.Type);
                    }
                    else
                    {
                        classNode.DeclaredFields.Add(l_expr);                    
                    }
                }
                else
                {
                    var exp = TypeCheck(classNode, methodNode, l_expr.Expression);

                    // Console.WriteLine("check for {0}:{1} [expression]", l_expr, l_expr.Type);
                    if (CheckClassExists(program, exp.Type))
                    {
                        if (!IsSubClassOf(classNode.Name, exp.Type))
                        {
                            Program.ReportError(l_expr.Expression.Context.Start, string.Format("Unable to access fields of class {0} from class {1}.", exp.Type, classNode.Name));
                        }
                        else
                        {
                            if (CheckSymbolTable(program.Classes[exp.Type].DeclaredFields, l_expr)) {}
                            else
                            {
                                // Console.WriteLine("class {0} doesn't have field {1}", exp.Type, l_expr.Name);
                                Program.ReportError(l_expr.Context.Start, string.Format("Class {0} does not contain a field '{1}'", exp.Type, l_expr.Name));
                            }                            
                        }                            
                    }                    
                }
            }
            else
            {
                if (CheckSymbolTable(methodNode.Args, l_expr))
                {

                }
                else if (CheckSymbolTable(methodNode.Variables, l_expr))
                {
                    // Console.WriteLine("found variable {0}:{1}", l_expr.Name, l_expr.Type);
                }
                else
                {
                    methodNode.Variables.Add(l_expr);                    
                }
            }
        }
        else if (stmt is LogicExpressionAstNode)
        {
            var lnode = stmt as LogicExpressionAstNode;
            TypeCheck(classNode, methodNode, lnode.Left);
            TypeCheck(classNode, methodNode, lnode.Right);
        }
        else if (stmt is MethodCallNode)
        {
            var mnode = stmt as MethodCallNode;
            if (mnode.Expression != null)
            {
                // Console.WriteLine("method call {0}:{1}", mnode.Name, mnode.Type);
                var exp = TypeCheck(classNode, methodNode, mnode.Expression);

                // Console.WriteLine(" {0} exp {1}:{2}", mnode.Name, exp.Name, exp.Type);
                if (exp.Type != "Bottom") {
                    if (!program.Classes.ContainsKey(exp.Type))
                    {
                        Program.ReportError(mnode.Context.Start, string.Format("Class '{0}' not found in class table.", exp.Type));
                    }
                    else
                    {
                        // typecheck args
                        foreach (var a in mnode.ActualArgs)
                        {
                            TypeCheck(classNode, methodNode, a);
                        }
                        if (!TypeCheck(program.Classes[mnode.Expression.Type], mnode))
                        {
                            Program.ReportError(mnode.Context.Start, string.Format("[method] Type '{0}' does not contain a definition for '{1}'", mnode.Expression.Type, mnode.Name));
                        }
                        // else 
                        // {
                        //     Console.WriteLine("method call has '{0}' method property", mnode.Method);
                        // }
                    }

                    // Console.WriteLine("set method {0} to type {1}", mnode.Name, mnode.Type);
                    stmt.Type = mnode.Type;
                }
            }
            else
            {
                Program.Debug(string.Format("            constructor call {0}", mnode.Name), 1);
                if (!program.Classes.ContainsKey(mnode.Name))
                {
                    Program.ReportError(mnode.Context.Start, string.Format("Invalid constructor call. Class '{0}' not found in class table.", mnode.Name));
                }
                else
                {
                    foreach (var a in mnode.ActualArgs)
                    {
                        Program.Debug(string.Format("                [{0}] constructor arg {1}:{2}", mnode.Name, a.Name, a.Type), 1);
                        TypeCheck(classNode, methodNode, a);
                    }
                    TypeCheck(classNode, mnode);
                }
            }

            // check for correct arguments to method call
        }
        else if (stmt is ReturnStatementAstNode)
        {
            var ret_stmt = stmt as ReturnStatementAstNode;

            if (ret_stmt.Expression != null)
            {
                Program.Debug(string.Format("            return expression {0}", ret_stmt.Expression), 1);
                var ret_expr = TypeCheck(classNode, methodNode, ret_stmt.Expression);
                // Console.WriteLine("return {0} expect {1} : {2}", ret_expr.Type, methodNode.Type, ret_expr);
                // TypeCheck(classNode, methodNode, ret_expr);
                if (!IsSubClassOf(ret_expr.Type, methodNode.Type))
                {
                    Program.ReportError(ret_expr.Context.Start, string.Format("Invalid return type '{0}', expected '{1}'",
                                        ret_expr.Type, methodNode.Type));
                }
                ret_stmt.Type = ret_expr.Type;
            }
            else
            {
                ret_stmt.Type = "Nothing";                    
                if (!IsSubClassOf("Nothing", methodNode.Type))
                {
                    Program.ReportError(ret_stmt.Context.Start, string.Format("Invalid return type '{0}', expected '{1}'",
                                        ret_stmt.Type, methodNode.Type));
                }
            }

            if (string.IsNullOrEmpty(methodNode.ReturnType))
            {
                // Console.WriteLine("[{0}] setting return type to {1}", methodNode.Name, methodNode.Type);
                methodNode.ReturnType = methodNode.Type;                
            }
        }
        else if (stmt is StringExpressionAstNode)
        {
            var snode = stmt as StringExpressionAstNode;

            if (snode.Type != "String")
            {
                Program.ReportError(snode.Context.Start, string.Format("String literal '{0}' with invalid type '{1}'", snode.Text, snode.Type));
            }
        }
        else if (stmt is WhileStatementAstNode)
        {
            var wnode = stmt as WhileStatementAstNode;
            var cond = TypeCheck(classNode, methodNode, wnode.Condition);

            if (cond.Type != "Boolean")
            {
                Program.ReportError(cond.Context.Start, string.Format("Cannot convert type '{0}' to 'Boolean'", wnode.Condition.Type));
            } 
            else 
            {
                for (var i = 0; i < 10 && Program.OtherErrors == 0; i++)
                {
                    // _whileChanged = false;
                    foreach (var s in wnode.Statements)
                    {
                        // Console.WriteLine(s);
                        TypeCheck(classNode, methodNode, s);
                    }                    
                    // Console.WriteLine("---- while changed: {0} ----", _whileChanged);
                }

            }
            stmt.Type = "Nothing";
        }
        else
        {
            Console.WriteLine("[method] other type: {0}", stmt);
        }

        // Console.WriteLine("Returning {0}", stmt);
        return stmt;
    }


    public bool TypeCheck(ClassAstNode classNode, MethodCallNode methodCallNode)
    {
        var found = false;

        if (classNode.Name == "_MAIN_" && methodCallNode.Name != "_MAIN_")
        {
            // main can call any class constructor
            foreach (var c in program.Classes)
            {
                if (c.Value.Name == methodCallNode.Name)
                {
                    TypeCheck(c.Value, methodCallNode);
                }
            }
        }
        else
        {
            foreach (var m in classNode.DeclaredMethods)
            {
                if (m.Name == methodCallNode.Name)
                {
                    found = true;
                    Program.Debug(string.Format("            check method call: {0}.{1}", classNode.Name, methodCallNode.Name), 1);

                    TypeCheck(m, methodCallNode);
                    methodCallNode.Method = m;
                    // Program.Debug(string.Format("            assign method call method: {0}.{1}", methodCallNode.Name, m.Name), 1);
                    methodCallNode.Type = m.Type;
                    break;
                }
            }            
            if (!found && !string.IsNullOrEmpty(classNode.Super))
            {
                found = TypeCheck(program.Classes[classNode.Super], methodCallNode);
            }
        }

        return found;
    }

    private void TypeCheck(MethodAstNode method, MethodCallNode call)
    {
        // Console.WriteLine("checking method {0}({1} args) for call {2}({3} args)", method.Name, method.Args.Count, call.Name, call.ActualArgs.Count);
        if (call.ActualArgs.Count != method.Args.Count)
        {
            Program.ReportError(call.Context.Start, string.Format("Incorrect number of arguments '{0}' for method '{1}' (expected {2})", call.ActualArgs.Count,
                method.Name, method.Args.Count));
        }
        else
        {
            for (var i = 0; i < call.ActualArgs.Count; i++)
            {
                if (!IsSubClassOf(call.ActualArgs[i].Type, method.Args[i].Type))
                {
                    if (call.Expression != null)
                    {
                        // Console.WriteLine("{0}.{1}({2})", call.Expression.Name, call.Name, call.ActualArgs);
                        Program.ReportError(call.ActualArgs[i].Context.Start, string.Format("Incorrect type for argument {0} of method call '{1}.{2}()'. Expected '{3}' but got '{4}:{5}'",
                                    i + 1, call.Expression.Name, call.Name, method.Args[i].Type, call.ActualArgs[i].Name, call.ActualArgs[i].Type));
                    }
                    else
                    {
                        // Console.WriteLine("{0}({1})", call.Name, call.ActualArgs);
                        Program.ReportError(call.ActualArgs[i].Context.Start, string.Format("Incorrect type for argument {0} of method call '{1}()'. Expected '{2}' but got '{3}:{4}'",
                                    i + 1, call.Name, method.Args[i].Type, call.ActualArgs[i].Name, call.ActualArgs[i].Type));
                    }
                }
            }
        }
    }
}
}
