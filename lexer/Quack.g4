grammar Quack;

@lexer::members{
public override IToken Emit()
{ 
        IToken result = base.Emit();
    switch (Type)
    {
      case SQ_STR_BDN:
        Program.ReportError(result, string.Format("Encountered newline in quoted string ('{0}')", Text.Replace("\n", "")));
        return result;
      case SQ_STR_BDE:
        Program.ReportError(result, string.Format("Illegal escape code in quoted string ('{0}')", Text));
        return result;
      default:
        return result;
    }
}
}

/*
 * Parser Rules
 */

// 3.2 STRUCTURE OF A QUACK PROGRAM
program         : class* statement* ;
class           : class_signature class_body ;
class_signature : CLASS name=IDENT LPAREN formal_args RPAREN ( EXTENDS super=IDENT )? ;
class_body      : LBRACE statement* method* RBRACE ;
formal_args     : ( IDENT COLON IDENT ( COMMA IDENT COLON IDENT )* )? ;
method          : DEF name=IDENT LPAREN args=formal_args RPAREN ( COLON type=IDENT )? body=statement_block ;
statement_block : LBRACE statement* RBRACE ;

// 4.1 CONTROL STRUCTURES
statement       : IF expr statement_block
                  ( ELIF expr statement_block )*
                  ( ELSE statement_block )?                                # ifStatement
                | WHILE cond=expr body=statement_block                     # whileStatement
                | left=l_expr ( COLON type=IDENT )? EQUAL right=expr ';'   # assignStatement
                | RETURN expr? ';'                                         # returnStatement
                | expr SEMI                                                # bareStatement
                ;

// 5   EXPRESSIONS
l_expr          : name=IDENT
                | expr DOT name=IDENT
                ;
expr            : text=(TQ_STR_LIT | SQ_STR_LIT | SQ_STR_BDE | SQ_STR_BDN) # stringExpr
                | value=INT_LIT                                            # integerExpr
                | name=IDENT                                               # identExpr // need to distinguish from l_expr
                | expr DOT name=IDENT                                      # dotIdentExpr // need to distinguish from l_expr
                | obj=expr DOT name=IDENT LPAREN args=actual_args RPAREN   # methodExpr
                | left=expr op=(TIMES | DIV) right=expr                    # infixExpr
                | left=expr op=(PLUS | MINUS) right=expr                   # infixExpr
                | left=expr op=EQ right=expr                               # comparisonExpr
                | left=expr op=LEQ right=expr                              # comparisonExpr
                | left=expr op=LT right=expr                               # comparisonExpr
                | left=expr op=GEQ right=expr                              # comparisonExpr
                | left=expr op=GT right=expr                               # comparisonExpr
                | left=expr op=AND right=expr                              # logicExpr
                | left=expr op=OR right=expr                               # logicExpr
                | op=NOT right=expr                                        # logicExpr
                | LPAREN expr RPAREN                                       # parensExpr
                | op=(TRUE | FALSE)                                        # boolExpr
                | name=IDENT LPAREN args=actual_args RPAREN                # constructorExpr
                ;
// 5.1 METHOD INVOCATION
actual_args     : ( expr ( COMMA expr )* )? ;


/*
 * Lexer Rules
 */
// 6.1 WHITE SPACE
WS        : (HT | NL | CR | SP)+ -> skip ;
HT        : '\t' ; // horizontal tab
NL        : '\n' ; // newline
CR        : '\r' ; // carriage return
SP        : ' ' ;  // space
// 6.2 KEYWORDS
CLASS     : 'class' ;
DEF       : 'def' ;
EXTENDS   : 'extends' ;
IF        : 'if' ;
ELIF      : 'elif' ;
ELSE      : 'else' ;
WHILE     : 'while' ;
RETURN    : 'return' ;
//     PREDEFINED IDENTIFIERS
//STRING    : 'String' ;
//INTEGER   : 'Int' ;
//OBJ       : 'Obj' ;
//BOOLEAN   : 'Boolean' ;
//NOTHING   : 'Nothing' ;
AND       : 'and' ;
OR        : 'or' ;
NOT       : 'not' ;
NONE      : 'none' ;
TRUE      : 'true' ;
FALSE     : 'false' ;
// 6.3 PUNCTUATION
//     ARITHMETIC OPERATORS
PLUS      : '+' ;
MINUS     : '-' ;
TIMES     : '*' ;
DIV       : '/' ;
//     COMPARISON
EQ        : '==' ; // EQUALS
LEQ       : '<=' ; // ATMOST
LT        : '<' ;  // LESS
GEQ       : '>=' ; // ATLEAST
GT        : '>' ;  // MORE
// AND, OR, NOT defined above
LBRACE    : '{' ;
RBRACE    : '}' ;
LPAREN    : '(' ;
RPAREN    : ')' ;
//LBRACK    : '[' ;
//RBRACK    : ']' ;
COMMA     : ',' ;
SEMI      : ';' ;
DOT       : '.' ;
COLON     : ':' ;
QUOTE     : '"' ;
EQUAL     : '=' ;
// 6.4 IDENTIFIERS
IDENT     : [a-zA-Z_][a-zA-Z0-9_]* ;
// 6.5 INTEGER LITERALS
INT_LIT   : [-]?[0-9]+ ;
// 6.6 STRING LITERALS
TQ_STR_LIT: '"""' .*? '"""'
            {
              var tqs = Text;
              tqs = tqs.Substring(3, tqs.Length - 6);
              Text = tqs;
            } ;
SQ_STR_LIT: ["] ([\\][0btnrf"] | ~["\r\n\\])* ["] 
            {
              var sqs = Text;
              sqs = sqs.Substring(1, sqs.Length - 2);
              Text = sqs;
            } ;
SQ_STR_BDE: ["] ([\\]~[0btnrf"] | ~["\r\n\\])* ["]
            {
              var sqs = Text;
              sqs = sqs.Substring(1, sqs.Length - 2);
              Text = sqs;
            } ;
SQ_STR_BDN: ["] (~["\r\n\\])* [\r\n]
            {
              var sqs = Text;
              sqs = sqs.Substring(1);
              Text = sqs;
            } ;
// 6.7 COMMENTS
COMMENT  : ('//' ~[\r\n]* | '/*' .*? '*/') -> skip ;
ERRORCHAR: . ; // catch all errors and pass to parser

