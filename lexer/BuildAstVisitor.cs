using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AntlrTest
{
internal class BuildAstVisitor : QuackBaseVisitor<AstNode>
{
    public override AstNode VisitProgram(QuackParser.ProgramContext context)
    {
        ProgramAstNode node = new ProgramAstNode();
        ClassAstNode newClass;
        StatementAstNode newStatement;

        // add built-in classes
        var objClass = new ClassAstNode { Name = "Obj" };
        objClass.DeclaredMethods.Add(new MethodAstNode { Name = "Obj", Type = "Obj", OwnerClass = "Obj" });
        objClass.DeclaredMethods.Add(new MethodAstNode { Name = "STR", Type = "String", OwnerClass = "Obj" });
        objClass.DeclaredMethods.Add(new MethodAstNode { Name = "PRINT", Type = "Nothing", OwnerClass = "Obj" });
        var objClassEqual = new MethodAstNode { Name = "EQUAL", Type = "Boolean", OwnerClass = "Obj" };
        objClassEqual.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Obj" });
        objClass.DeclaredMethods.Add(objClassEqual);
        node.Classes.Add("Obj", objClass);

        var intClass = new ClassAstNode { Name = "Int", Super = "Obj" };
        intClass.DeclaredMethods.Add(new MethodAstNode { Name = "Int", Type = "Int" });
        intClass.DeclaredMethods.Add(new MethodAstNode { Name = "STR", Type = "String", OwnerClass = "Int" });
        var intClassEquals = new MethodAstNode { Name = "EQUAL", Type = "Boolean" };
        intClassEquals.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Obj" });
        intClass.DeclaredMethods.Add(intClassEquals);
        var intClassPlus = new MethodAstNode { Name = "PLUS", Type = "Int" };
        intClassPlus.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Int" });
        intClass.DeclaredMethods.Add(intClassPlus);
        var intClassMinus = new MethodAstNode { Name = "MINUS", Type = "Int" };
        intClassMinus.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Int" });
        intClass.DeclaredMethods.Add(intClassMinus);
        var intClassTimes = new MethodAstNode { Name = "TIMES", Type = "Int" };
        intClassTimes.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Int" });
        intClass.DeclaredMethods.Add(intClassTimes);
        var intClassDivide = new MethodAstNode { Name = "DIVIDE", Type = "Int" };
        intClassDivide.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Int" });
        intClass.DeclaredMethods.Add(intClassDivide);
        var intClassAtLeast = new MethodAstNode { Name = "ATLEAST", Type = "Boolean" };
        intClassAtLeast.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Int" });
        intClass.DeclaredMethods.Add(intClassAtLeast);
        var intClassMore = new MethodAstNode { Name = "MORE", Type = "Boolean" };
        intClassMore.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Int" });
        intClass.DeclaredMethods.Add(intClassMore);
        var intClassLess = new MethodAstNode { Name = "LESS", Type = "Boolean" };
        intClassLess.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Int" });
        intClass.DeclaredMethods.Add(intClassLess);
        var intClassAtMost = new MethodAstNode { Name = "ATMOST", Type = "Boolean" };
        intClassAtMost.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Int" });
        intClass.DeclaredMethods.Add(intClassAtMost);
        node.Classes.Add("Int", intClass);

        var strClass = new ClassAstNode { Name = "String", Super = "Obj" };
        strClass.DeclaredMethods.Add(new MethodAstNode { Name = "String", Type = "String" });
        strClass.DeclaredMethods.Add(new MethodAstNode { Name = "STR", Type = "String", OwnerClass = "String" });
        strClass.DeclaredMethods.Add(new MethodAstNode { Name = "PRINT", Type = "Nothing", OwnerClass = "String" });
        var strClassPlus = new MethodAstNode { Name = "PLUS", Type = "String" };
        strClassPlus.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "String" });
        strClass.DeclaredMethods.Add(strClassPlus);
        node.Classes.Add("String", strClass);

        var boolClass = new ClassAstNode { Name = "Boolean",  Super = "Obj" };
        boolClass.DeclaredMethods.Add(new MethodAstNode { Name = "Boolean", Type = "Boolean" });
        boolClass.DeclaredMethods.Add(new MethodAstNode { Name = "STR", Type = "String", OwnerClass = "Boolean" });
        // var boolClassOr = new MethodAstNode { Name = "OR", Type = "Boolean", OwnerClass = "Boolean" };
        // boolClassOr.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Boolean" });
        // boolClass.DeclaredMethods.Add(boolClassOr);
        // var boolClassNot = new MethodAstNode { Name = "NOT", Type = "Boolean", OwnerClass = "Boolean" };
        // boolClassNot.Args.Add(new LeftExpressionAstNode { Name = "other", Type = "Boolean" });
        // boolClass.DeclaredMethods.Add(boolClassNot);
        node.Classes.Add("Boolean", boolClass);

        var nothingClass = new ClassAstNode { Name = "Nothing",  Super = "Obj" };
        nothingClass.DeclaredMethods.Add(new MethodAstNode { Name = "Nothing", Type = "Nothing" });
        node.Classes.Add("Nothing", nothingClass);

        foreach (var c in context.@class())
        {
            newClass = (ClassAstNode)Visit(c);
            if (newClass == null) continue;

            // check ProgramAstNode for existing class of the same name
            if (node.Classes.ContainsKey(newClass.Name))
            {
                var original = node.Classes[newClass.Name];
                Program.ReportError(c.class_signature().name, string.Format("The program already contains a definition for '{0}'",
                                    newClass.Name, original.Context.class_signature().name.Line, original.Context.class_signature().name.Column));
                Program.ReportError(original.Context.class_signature().name, "(location of the original definition related to the previous error)");
            }
            else
            {
                node.Classes.Add(newClass.Name, newClass);
            }
        }

        var main = new ClassAstNode { Name = "_MAIN_", Super = "Obj", Type = "Nothing" };
        var mainConstructor = new MethodAstNode();

        foreach (var s in context.statement())
        {
            newStatement = (StatementAstNode)Visit(s);
            mainConstructor.Statements.Add(newStatement);
        }
        mainConstructor.Name = main.Name;
        mainConstructor.Type = main.Type;
        mainConstructor.ReturnType = main.Type;
        main.DeclaredMethods.Add(mainConstructor);
        node.Classes.Add(main.Name, main);

        return node;
    }

    public override AstNode VisitClass(QuackParser.ClassContext context)
    {
        var node = new ClassAstNode();
        var constructor = new MethodAstNode();

        if (context.class_signature().super != null) { node.Super = context.class_signature().super.Text; }
        else { node.Super = "Obj"; }

        // // don't allow extending of built-in classes
        // if ((new string[] {"String", "Int", "Boolean", "Nothing"}).Contains(node.Super))
        // {
        //     Program.OtherErrors++;
        //     Program.ReportError(context.class_signature().super, string.Format("Extending the built-in class '{0}' is not permitted.", node.Super));
        //     return null;
        // }

        // args come in pairs of identifiers (name/type)
        for (var i = 0; i < context.class_signature().formal_args().IDENT().Count() - 1; i += 2)
        {
            var arg = new  LeftExpressionAstNode
            {
                Name = context.class_signature().formal_args().IDENT()[i].Symbol.Text,
                Type = context.class_signature().formal_args().IDENT()[i + 1].Symbol.Text,
                DeclaredType = context.class_signature().formal_args().IDENT()[i + 1].Symbol.Text,
                Context = context.class_signature().formal_args() // should really be context of the individual args
            };
            constructor.Args.Add(arg);
        }

        // any statements in the class body are part of the constructor
        foreach (var s in context.class_body().statement())
        {
            var snode = Visit(s);
            constructor.Statements.Add((StatementAstNode)snode);
        }

        node.Name = context.class_signature().name.Text;
        node.Context = context;

        constructor.Name = node.Name;
        constructor.Type = node.Name;
        constructor.ReturnType = node.Name;
        node.DeclaredMethods.Add(constructor);

        foreach (var m in context.class_body().method())
        {
            var newMethod = (MethodAstNode)Visit(m);
            var exists = false;
            // check class body for existing method of the same name
            foreach (var method in node.DeclaredMethods)
            {
                if (method.Name == newMethod.Name)
                {
                    Program.ReportError(m.name, string.Format("Method '{0}()' is already defined.", newMethod.Name));
                    // Program.ReportError(method.Context.name, "(location of the original definition related to the previous error)");
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                node.DeclaredMethods.Add(newMethod);
            }
        }

        return node;
    }

    public override AstNode VisitAssignStatement(QuackParser.AssignStatementContext context)
    {
        var node = new AssignStatementAstNode();
        node.Left = (ExpressionAstNode)Visit(context.left);
        node.Right = (ExpressionAstNode)Visit(context.right);

        if (context.type != null)
        {
            node.Type = context.type.Text;
            // Console.WriteLine("declared type {0}:{1}", node.Left.Name, context.type.Text);
        }
        // Console.WriteLine("Assign {0} : {1} = {2} : {3}", node.Left.Name, node.Left.Type, node.Right.Name, node.Right.Type);

        if (node.Left.Type == "Bottom" && !string.IsNullOrEmpty(node.Right.Type))
        {
            node.Left.Type = node.Right.Type;
        }

        node.Context = context;
        return node;
    }

    public override AstNode VisitBareStatement(QuackParser.BareStatementContext context)
    {
        return (ExpressionAstNode)Visit(context.expr());
    }

    public override AstNode VisitIfStatement(QuackParser.IfStatementContext context)
    {
        var node = new IfStatementAstNode { Context = context };
        ExpressionAstNode cond;
        for (var i = 0; i < context.statement_block().Count(); i++)
        {
            var branch = new IfBranch();

            if (context.expr().Count() > i)
            {
                cond = (ExpressionAstNode)Visit(context.expr()[i]);
                // Console.WriteLine("cond {0}.{1}:{2}", ((MethodCallNode)cond).Expression, cond.Name, cond.Type);
            }
            else
            {
                cond = new BooleanExpressionAstNode { Value = true, Type = "Boolean", Name = "[else branch]" };
            }
            branch.Condition = cond;

            foreach (var s in context.statement_block()[i].statement())
            {
                var snode = Visit(s);
                branch.Statements.Add((StatementAstNode)snode);
            }

            node.Branches.Add(branch);
        }

        return node;
    }

    public override AstNode VisitReturnStatement(QuackParser.ReturnStatementContext context)
    {
        var node = new ReturnStatementAstNode();

        if (context.expr() != null)
        {
            node.Expression = (ExpressionAstNode)Visit(context.expr());
            // Console.WriteLine("Return {0}:{1}", node.Expression.Name, node.Expression.Type);
        }
        node.Context = context;

        return node;
    }

    public override AstNode VisitWhileStatement(QuackParser.WhileStatementContext context)
    {
        var node = new WhileStatementAstNode { Type = "Nothing" };
        node.Condition = (ExpressionAstNode)Visit(context.cond);

        foreach (var s in context.statement_block().statement())
        {
            var snode = Visit(s);
            node.Statements.Add((StatementAstNode)snode);
        }
        //Console.WriteLine("while statement has condition {0}", node.Condition);
        // node.Body = (StatementBlockNode)Visit(context.statement_block());

        return node;
    }

    public override AstNode VisitMethod(QuackParser.MethodContext context)
    {
        var node = new MethodAstNode { Name = context.name.Text };

        if (context.type != null)
        {
            node.Type = context.type.Text;
            node.ReturnType = context.type.Text;
        }
        else
        {
            node.Type = "Nothing";
        }
        // Console.WriteLine("visit method {0}:{1}", node.Name, node.Type);
        for (var i = 0; i < context.formal_args().IDENT().Count() - 1; i += 2)
        {
            node.Args.Add(new LeftExpressionAstNode
            {
                Name = context.formal_args().IDENT()[i].Symbol.Text,
                Type = context.formal_args().IDENT()[i + 1].Symbol.Text,
                DeclaredType = context.formal_args().IDENT()[i + 1].Symbol.Text,
                Context = context.formal_args() // should really be context of the individual args
            });
        }

        foreach (var s in context.statement_block().statement())
        {
            var snode = Visit(s);
            node.Statements.Add((StatementAstNode)snode);
        }
        node.Context = context;

        return node;
    }

    public override AstNode VisitL_expr(QuackParser.L_exprContext context)
    {
        var node = new LeftExpressionAstNode { Name = context.name.Text, Type = "Bottom", Context = context };

        // Console.WriteLine("left expression: {0} : {1}", node.Name, node.Type);
        if (context.expr() != null)
        {
            node.Expression = (ExpressionAstNode)Visit(context.expr());
        }
        node.Identifier = context.name;
        return node;
    }

    public override AstNode VisitIdentExpr(QuackParser.IdentExprContext context)
    {
        var node = new IdentifierExpressionAstNode { Name = context.name.Text, Type = "Bottom", Context = context };
        return node;
    }

    public override AstNode VisitDotIdentExpr(QuackParser.DotIdentExprContext context)
    {
        var node = new DotIdentifierExpressionAstNode { Name = context.name.Text, Context = context };
        var exp = (ExpressionAstNode)Visit(context.expr());
        node.Expression = exp;
        node.Identifier = context.name;

        return node;
    }

    public override AstNode VisitStringExpr(QuackParser.StringExprContext context)
    {
        var node = new StringExpressionAstNode { Name = "[string literal]", Text = context.text.Text, Type = "String", Context = context };
        return node;
    }

    public override AstNode VisitIntegerExpr(QuackParser.IntegerExprContext context)
    {
        var node = new IntegerExpressionAstNode { Name = "[int literal]", Value = int.Parse(context.value.Text), Type = "Int", Context = context };
        return node;
    }

    public override AstNode VisitComparisonExpr(QuackParser.ComparisonExprContext context)
    {
        var node = new MethodCallNode();
        switch (context.op.Type)
        {
            case QuackLexer.EQ: node.Name = "EQUAL"; break;
            case QuackLexer.LEQ: node.Name = "ATMOST"; break;
            case QuackLexer.LT: node.Name = "LESS"; break;
            case QuackLexer.GEQ: node.Name = "ATLEAST"; break;
            case QuackLexer.GT: node.Name = "MORE"; break;
            default: throw new NotSupportedException();
        }
        node.Expression = (ExpressionAstNode)Visit(context.left);
        node.ActualArgs.Add((ExpressionAstNode)Visit(context.right));
        node.Context = context;

        return node;
    }

    public override AstNode VisitLogicExpr(QuackParser.LogicExprContext context)
    {
        var node = new LogicExpressionAstNode();
        var right = (ExpressionAstNode)Visit(context.right);
        switch (context.op.Type)
        {
        case QuackLexer.AND: 
            node.Left = (ExpressionAstNode)Visit(context.left); 
            node.Name = "AND";
            break;
        case QuackLexer.OR: 
            node.Left = (ExpressionAstNode)Visit(context.left);
            node.Name = "OR";
            break;
        case QuackLexer.NOT:
            node.Left = right;
            node.Name = "NOT";
            break;
        default: throw new NotSupportedException();
        }
        node.Right = right;
        node.Context = context;
        node.Type = "Boolean";

        return node;
    }

    public override AstNode VisitBoolExpr(QuackParser.BoolExprContext context)
    {
        var node = new BooleanExpressionAstNode { Type = "Boolean", Name = "[bool literal]" };

        if (context.op.Type == QuackLexer.TRUE) {
            node.Value = true;
        }
        else {
            node.Value = false;
        }

        return node;
    }

    public override AstNode VisitParensExpr(QuackParser.ParensExprContext context)
    {
        // Console.WriteLine("parens node");
        return Visit(context.expr());
    }

    public override AstNode VisitInfixExpr(QuackParser.InfixExprContext context)
    {
        var node = new MethodCallNode();
        switch (context.op.Type)
        {
        case QuackLexer.PLUS: node.Name = "PLUS"; break;
        case QuackLexer.MINUS: node.Name = "MINUS"; break;
        case QuackLexer.TIMES: node.Name = "TIMES"; break;
        case QuackLexer.DIV: node.Name = "DIVIDE"; break;
        default: throw new NotSupportedException();
        }
        node.Expression = (ExpressionAstNode)Visit(context.left);
        node.ActualArgs.Add((ExpressionAstNode)Visit(context.right));
        node.Context = context;

        return node;
    }

    public override AstNode VisitConstructorExpr(QuackParser.ConstructorExprContext context)
    {
        var node = new MethodCallNode { Name = context.name.Text, Context = (QuackParser.ExprContext)context, Type = context.name.Text };
        for (var i = 0; i < context.args.expr().Count(); i++)
        {
            var arg = (ExpressionAstNode)Visit(context.args.expr()[i]);
            node.ActualArgs.Add(arg);
        }

        return node;
    }

    public override AstNode VisitMethodExpr(QuackParser.MethodExprContext context)
    {
        var node = new MethodCallNode { Context = context };
        node.Expression = (ExpressionAstNode)Visit(context.obj);
        node.Name = context.name.Text;

        for (var i = 0; i < context.args.expr().Count(); i++)
        {
            var arg = (ExpressionAstNode)Visit(context.args.expr()[i]);
            node.ActualArgs.Add(arg);
        }
        node.Type = "Bottom";

        return node;
    }
}
}
