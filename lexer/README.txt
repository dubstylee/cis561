Brian Williams
CIS561

My compiler handles about 95% of the situations that I have come up with. However, one thing it definitely doesn't do
correctly is check variable declaration before use in all branches. It treats an entire method as one scope, regardless
of the control flow.

It does require a return statement in every branch of an if statement (if any statement contains a return statement),
but return statements are optional for methods with a return type of Nothing. Nothing is also the return type of the empty
statement 'return;'

Subclasses are required to declare all fields of the same name and type.

Overridden methods must have the same number of arguments with the same name and compatible types, as well as a compatible
return type.

My compiler does not allow 'this' as a standalone keyword. It can only be used as an expression that is part of a dot
identifier. For example, 'this.x;' is allowed, but 'return this;' is not. Fields are protected. That is, they are only
available within methods of the same class.

Negative integers are supported. Type inferrence works properly, insofar as the compiler will allow a type to be changed
to the least common ancestor, except in the case when there is a declared type. For example, 'x = 4; x = "str";' is acceptable
and results in the variable x of type Obj. However, 'x:Int = 4; x = "str";' is not allowed, because x is declared to have
a type of Int.

Any errors from the scanner will be reported, but will not prevent the parser from running and therefore any lexical errors
will most likely result in two separate errors. The only lexical errors that are specifically handled are a bad escape
character in a single quoted string and a newline in a single quoted string.

There are still a couple tests that cause the compiler to crash, but certainly nothing in the provided tests should fail. My
compiler will also not fold my laundry. That is a known fault.

Many of my test files have been adapted (or directly used from) the test suites provided by Andrew or the sample Quack files.

There may still be some remnants of debugging logs that produce output to the console, but any errors should output to stderr.
Output from the executed program will output to the console.

Short circuit logic works, and code is generated using goto/labels. However, there are some situations which have not
been handled, such as nested logic expressions (x and y or y and x).
------------------------------------------

USAGE: (to test one file at a time)

./quack <quack file> 
will generate a C file called generated.c (unless there are any errors prior to code generation)

make generated
will compile the generated C code with the Builtins.c and produce an output file called a.out


OR: (to test a whole directory at once)

There is a script titled do_tests that can be run on an entire directory

./do_tests good
will compile and run all Quack files within the directory 'good'
