using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AntlrTest
{
    class Program
    {
        private static void Main(string[] args)
        {
            if (args.Count() < 1)
            {
                Console.Error.WriteLine("Usage: scanner.exe <input file>");
            }
            else
            {
                // throw away any extra args
                ParseString(args[0]);
            }
        }

        private static void ParseString(string inputFile)
        {
            if (File.Exists(inputFile))
            {
                using (var fs = File.OpenRead(inputFile))
                {
                    var inputStream = new AntlrInputStream(fs);
                    var lexer = new QuackLexer(inputStream);
                    var tokenStream = new CommonTokenStream(lexer);
                    var parser = new QuackParser(tokenStream);
                    //parser.BuildParseTree = true;
                    var tree = parser.program();
Console.WriteLine("tree: " + tree.ToStringTree(parser));
//ParseTreeWalker.Default.Walk(new MyQuackListener(), tree);
                    Console.WriteLine("{0} errors.", parser.NumberOfSyntaxErrors);
                }
            } 
            else 
            {
                Console.Error.WriteLine("File not found: '{0}'", inputFile);
            }
        }
    }
}
