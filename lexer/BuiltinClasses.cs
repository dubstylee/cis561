﻿using System;

namespace AntlrTest
{
    public class Obj
    {
        //bool EQUAL(Obj other);
        string STR() { return "Obj"; }
        string PRINT() { return "Print"; }
    }

    public class Int : Obj
    {
        //int PLUS(Int b);
    }

    public class Boolean : Obj
    {
        public bool Value { get; set; }
    }

    public class String : Obj
    {

    }

    public class Nothing : Obj
    {
        string STR() { return "Nothing"; }
    }
}
