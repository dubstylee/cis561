using Antlr4.Runtime;
using System.Collections.Generic;

namespace AntlrTest
{
    public abstract class AstNode
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public ParserRuleContext Context { get; set; }
    }

    public class ProgramAstNode : AstNode
    {
        public Dictionary<string, ClassAstNode> Classes { get; set; }

        public ProgramAstNode()
        {
            Classes = new Dictionary<string, ClassAstNode>();
        }
    }

    public class ClassAstNode : AstNode
    {
        public string Super { get; set; }

        // fields are added at type checking
        public List<LeftExpressionAstNode> Fields { get; set; }
        public List<LeftExpressionAstNode> DeclaredFields { get; set; }
        public List<MethodAstNode> Methods { get; set; }
        public List<MethodAstNode> DeclaredMethods { get; set; }

        public new QuackParser.ClassContext Context { get; set; }
        public bool ErrorDetected { get; set; }

        public ClassAstNode()
        {
            Fields = new List<LeftExpressionAstNode>();
            DeclaredFields = new List<LeftExpressionAstNode>();
            DeclaredMethods = new List<MethodAstNode>();
            Methods = new List<MethodAstNode>();
        }
    }

    public abstract class StatementAstNode : AstNode { }

    public class MethodAstNode : AstNode
    {
        public List<LeftExpressionAstNode> Args { get; set; } // formal_args

        public List<LeftExpressionAstNode> Variables { get; set; }
        public List<StatementAstNode> Statements { get; set; }
        public new QuackParser.MethodContext Context { get; set; }

        public string OwnerClass { get; set; }
        public string ReturnType { get; set; }

        public MethodAstNode()
        {
            Args = new List<LeftExpressionAstNode>();
            Variables = new List<LeftExpressionAstNode>();
            Statements = new List<StatementAstNode>();
        }
    }

    public class IfBranch
    {
        public ExpressionAstNode Condition { get; set; }
        public List<StatementAstNode> Statements { get; set; }
        public bool HasReturn { get; set; }

        public IfBranch()
        {
            Condition = new ExpressionAstNode();
            Statements = new List<StatementAstNode>();
        }
    }

    public class IfStatementAstNode : StatementAstNode
    {
        public List<IfBranch> Branches { get; set; }
        public bool HasReturn { get; set; }

        public IfStatementAstNode()
        {
            Branches = new List<IfBranch>();
        }
    }

    public class WhileStatementAstNode : StatementAstNode
    {
        public ExpressionAstNode Condition { get; set; }
        public List<StatementAstNode> Statements { get; set; }

        public WhileStatementAstNode()
        {
            Statements = new List<StatementAstNode>();
        }
    }

    public class AssignStatementAstNode : StatementAstNode
    {
        public ExpressionAstNode Left { get; set; }
        public ExpressionAstNode Right { get; set; }
        public new QuackParser.AssignStatementContext Context { get; set; }
    }

    public class ReturnStatementAstNode : StatementAstNode
    {
        public ExpressionAstNode Expression { get; set; }
    }

    public class ExpressionAstNode : StatementAstNode
    {
        public string DeclaredType { get; set; }
    }

    public class LeftExpressionAstNode : ExpressionAstNode
    {
        public ExpressionAstNode Expression { get; set; }
        public IToken Identifier { get; set; }
    }

    public class StringExpressionAstNode : ExpressionAstNode
    {
        public string Text { get; set; }
    }

    public class IntegerExpressionAstNode : ExpressionAstNode
    {
        public int Value { get; set; }
    }

    public class BooleanExpressionAstNode : ExpressionAstNode
    {
        public bool Value { get; set; }
    }

    public class LogicExpressionAstNode : ExpressionAstNode
    {
        public ExpressionAstNode Left { get; set; }
        public ExpressionAstNode Right { get; set; }
    }

    public class IdentifierExpressionAstNode : ExpressionAstNode {}
    public class DotIdentifierExpressionAstNode : ExpressionAstNode
    {
        public ExpressionAstNode Expression { get; set; }
        public IToken Identifier { get; set; }
    }

    public class MethodCallNode : ExpressionAstNode
    {
        public ExpressionAstNode Expression { get; set; }
        public List<ExpressionAstNode> ActualArgs { get; set; }
        public MethodAstNode Method { get; set; }

        public MethodCallNode()
        {
            ActualArgs = new List<ExpressionAstNode>();
        }
    }
}
