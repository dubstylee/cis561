using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AntlrTest
{
public class Program
{
    public static int OtherErrors = 0;
    private static string FileName = "";
    // 0 = off
    // 1 = display type-checking
    // 2 = display type-checking + post-check tree
    // 3 = display type-checking + pre-check tree + post-check tree
    private static int DEBUG_LEVEL = 0;

    private static ProgramAstNode ast;

    private static void Main(string[] args)
    {
        if (args.Count() < 1)
        {
            Console.Error.WriteLine("Usage: quack.exe <input file>");
        }
        else
        {
            FileName = args[0];
            // throw away any extra args
            ParseString(FileName);
        }
    }

    public static bool IsSubClassOf(string left, string right)
    {
        // Console.WriteLine("{0} <= {1}??", left, right);
        // we know the classes are valid at this point
        var retval = false;

        if (left == "Bottom" || right == "Bottom")
        {
            retval = false;
        }
        else if (left == right)
        {
            retval = true;
        }
        else
        {
            if (!(left == "Obj"))
            {
                //Console.WriteLine("left {0} parent {1}", left, ast.Classes[left].Super);
                retval = IsSubClassOf(ast.Classes[left].Super, right);
            }
        }

        return retval;
    }

    public static void Debug(string message, int level, bool newline = true)
    {
        if (DEBUG_LEVEL >= level)
        {
            if (newline) { Console.WriteLine(message); }
            else { Console.Write(message); }
        }
    }

    public static void DisplayProgram(ProgramAstNode node, int level = 3)
    {
        Debug(string.Format("Program: {0}", FileName), level);
        foreach (var classNode in node.Classes)
        {
            Debug(string.Format("  Class: {0}", classNode.Value.Name), level, false);
            if (!string.IsNullOrEmpty(classNode.Value.Super))
            {
                Debug(string.Format(" ({0})", classNode.Value.Super), level, false);
            }
            Debug(string.Empty, level);
            foreach (var field in classNode.Value.Fields)
            {
                Debug(string.Format("    [field] {0}:{1}", field.Name, field.Type), level);
            }
            foreach (var methodNode in classNode.Value.Methods)
            {
                var args = "";
                Debug(string.Format("    Method: {0}.{1} (", methodNode.OwnerClass, methodNode.Name), level, false);
                foreach (var arg in methodNode.Args)
                {
                    if (args != string.Empty) args += ", ";
                    args += string.Format("{0}:{1}", arg.Name, arg.Type);
                }
                Debug(string.Format("{0}) : {1}", args, methodNode.Type), level);
                foreach (var variable in methodNode.Variables)
                {
                    Debug(string.Format("      [var]   {0}:{1}", variable.Name, variable.Type), level);
                }
                foreach (var stmt in methodNode.Statements)
                {
                    if (stmt is AssignStatementAstNode)
                    {
                        var anode = stmt as AssignStatementAstNode;
                        Debug(string.Format("      Statement: {0}:{1} = {2}:{3}", anode.Left, anode.Left.Type, anode.Right, anode.Right.Type), level);
                    }
                    else if (stmt != null)
                    {
                        Debug(string.Format("      Statement: {0} : {1}", stmt, stmt.Type), level);
                    }
                }
            }
        }
    }

    public static void ReportError(IToken startToken, string errorMessage)
    {
        // ignore > 10 errors
        if (OtherErrors < 10)
        {
            OtherErrors++;
            if (startToken != null)
            {
                Console.Error.WriteLine(string.Format("{0}({1},{2}): {3}", FileName, startToken.Line, startToken.Column, errorMessage));
            }
            else
            {
                Console.Error.WriteLine(string.Format("Quack: {0}", errorMessage));
            }
        }
    }

    private static void ValidateClass(ClassAstNode node)
    {
        var seen = new List<string>();
        var startNode = node; // for error reporting

        if (node.Name != "Obj")
        {
            if (!ast.Classes.ContainsKey(node.Super))
            {
                ReportError(startNode.Context.class_signature().super, string.Format("{0} has an invalid parent class: {1} not found in class table.", startNode.Name, startNode.Super));
            }
            else
            {
                do
                {
                    if (node.Super == "Obj") break;
                    seen.Add(node.Name);
                    if (seen.Contains(node.Super))
                    {
                        foreach (var c in seen)
                        {
                            if (ast.Classes.TryGetValue(c, out node))
                            {
                                node.ErrorDetected = true;
                            }
                        }

                        node = startNode;
                        var depends = node.Name;
                        while (!depends.Contains(node.Super))
                        {
                            node = ast.Classes[node.Super];
                            depends += "->" + node.Name;
                        }
                        depends += "->" + node.Super;
                        ReportError(startNode.Context.class_signature().name, string.Format("Circular dependency detected: {0}", depends));
                        break;
                    }

                    if (!ast.Classes.ContainsKey(node.Super))
                    {

                        ReportError(startNode.Context.class_signature().name, string.Format("Invalid class detected: {0} has no valid path to Obj.", startNode.Name));
                        break;
                    }
                }
                while (ast.Classes.TryGetValue(node.Super, out node));
            }

            if (startNode != null)  // don't check built-in classes
            {
                foreach (var m in startNode.DeclaredMethods)
                {
                    ValidateMethod(m);
                }
            }
        }
    }

    private static void ValidateMethod(MethodAstNode method)
    {
        foreach (var s in method.Statements)
        {
            ValidateStatement(s);
        }
    }

    private static void ValidateStatement(StatementAstNode statement)
    {
        if (statement is ReturnStatementAstNode)
        {
            var rnode = statement as ReturnStatementAstNode;
            if (rnode.Expression != null)
            {
                ValidateStatement(rnode.Expression);
            }
        }
        else if (statement is IfStatementAstNode)
        {
            var inode = statement as IfStatementAstNode;
            foreach (var b in inode.Branches)
            {
                // evaluate each expression
                ValidateStatement(b.Condition);

                // evaluate statement block
                foreach (var s in b.Statements)
                {
                    ValidateStatement(s);
                }
            }
        }
        else if (statement is WhileStatementAstNode)
        {
            var wnode = statement as WhileStatementAstNode;
            ValidateStatement(wnode.Condition);

            foreach (var s in wnode.Statements)
            {
                ValidateStatement(s);
            }
        }
        else if (statement is AssignStatementAstNode)
        {
            var anode = statement as AssignStatementAstNode;
            if (anode.Left != null)
            {
                ValidateStatement(anode.Left);
            }
            if (anode.Right != null)
            {
                ValidateStatement(anode.Right);
            }
        }
        else if (statement is MethodCallNode)
        {
            var mnode = statement as MethodCallNode;
            if (mnode.Expression != null)
            {
                ValidateStatement(mnode.Expression);
            }
        }
    }

    private static void CheckInheritedFields(ClassAstNode classNode)
    {
        var found = false;
        // Console.WriteLine(":checking inherited fields {0}", classNode.Name);
        
        for (var i = 0; i < classNode.Fields.Count; i++)
        {
            // Console.WriteLine(":checking inherited field {0}:{1}", classNode.Fields[i].Name, classNode.Fields[i].Type);
            found = false;
            for (var j = 0; j < classNode.DeclaredFields.Count; j++)
            {
                if (classNode.Fields[i].Name == classNode.DeclaredFields[j].Name)
                {
                    found = true;
                    if (!IsSubClassOf(classNode.DeclaredFields[j].Type, classNode.Fields[i].Type))
                    {
                        Program.ReportError(classNode.DeclaredFields[j].Context.Start, string.Format("Field {0} has incorrect type, expected {1}",
                                classNode.DeclaredFields[j].Name, classNode.Fields[i].Type));         
                    }
                    // Console.WriteLine("same name field, check type");
                }
            }

            if (!found)
            {
                Program.ReportError(classNode.Context.Start, string.Format("Class {0} does not implement inherited field '{1}'", classNode.Name, classNode.Fields[i].Name));
            }
        }

         // Console.WriteLine(":checking declared fields {0} ({1})", classNode.Name, classNode.DeclaredFields.Count);
        for (var k = 0; k < classNode.DeclaredFields.Count; k++)
        {
            // Console.WriteLine("checking declared field {0}:{1}", classNode.DeclaredFields[k].Name, classNode.DeclaredFields[k].Type);
            found = false;
            for (var l = 0; l < classNode.Fields.Count; l++)
            {
                if (classNode.Fields[l].Name == classNode.DeclaredFields[k].Name)
                {
                    found = true;
                    // Console.WriteLine("already has field {0}", classNode.Fields[l].Name);
                }
            }

            if (!found)
            {
                classNode.Fields.Add(classNode.DeclaredFields[k]);
            }
        }

    }

    private static void CheckInheritedMethod(ClassAstNode classNode, MethodAstNode methodNode)
    {
        var error = false;
        var found = false;

        methodNode.OwnerClass = classNode.Name;
        if (methodNode.Name == classNode.Name)
        {
            found = true;
            classNode.Methods[0] = methodNode;
            // Console.WriteLine("replacing constructor with {0}", methodNode.Name);
        }
        else
        {
            for (var i = 0; i < classNode.Methods.Count; i++)
            {
                error = false;
                found = false;
                if (classNode.Methods[i].Name == methodNode.Name)
                {
                    found = true;
                    if (classNode.Methods[i].Args.Count != methodNode.Args.Count)
                    {
                        error = true;
                        ReportError(methodNode.Context.Start, string.Format("Incorrect number of arguments in inherited method {0}", methodNode.Name));
                    }
                    else
                    {
                        for (var j = 0; j < classNode.Methods[i].Args.Count; j++)
                        {
                            // classNode.Methods is the superclass method
                            if (!IsSubClassOf(methodNode.Args[j].Type, classNode.Methods[i].Args[j].Type) || 
                                classNode.Methods[i].Args[j].Name != methodNode.Args[j].Name)
                            {
                                error = true;
                                ReportError(methodNode.Args[j].Context.Start, string.Format("Inherited method {0} has invalid argument {1}:{2} (expected {3}:{4})", methodNode.Name, methodNode.Args[j].Name, methodNode.Args[j].Type,
                                                    classNode.Methods[i].Args[j].Name, classNode.Methods[i].Args[j].Type));
                                break;
                            }
                        }                    
                    }

                    // classNode.Methods is the superclass method
                    if (!IsSubClassOf(methodNode.Type, classNode.Methods[i].Type))
                    {
                        error = true;
                        ReportError(methodNode.Context.Start, string.Format("Inherited method {0} has invalid return type {1} (expected {2} or a subclass)", 
                                            methodNode.Name, methodNode.Type, classNode.Methods[i].Type));
                        break;
                    }

                    if (!error)
                    {
                        // Console.WriteLine("overriding method {0}", methodNode.Name);
                        classNode.Methods[i] = methodNode;
                        break;
                    }
                }
            }
        }


        if (!found)
        {
            // Console.WriteLine("adding method {0}", methodNode.Name);
            classNode.Methods.Add(methodNode);
        }
    }

    private static void InheritStuff(ClassAstNode node)
    {
        var children = ast.Classes.Where(c => c.Value.Super == node.Name);
        foreach (var child in children)
        {
            // Console.WriteLine("class {0} has {1} fields", node.Name, node.Fields.Count);
            child.Value.Fields = new List<LeftExpressionAstNode>(node.Fields);
            child.Value.Methods = new List<MethodAstNode>(node.Methods);

            // foreach (var f in child.Value.DeclaredFields)
            // {
                CheckInheritedFields(child.Value);
            // }
             //   child.Value.Methods[0] = child.Value.DeclaredMethods[0];
            foreach (var m in child.Value.DeclaredMethods)
            {
                CheckInheritedMethod(child.Value, m);
            }
            InheritStuff(child.Value);
        }
    }

    private static void ParseString(string inputFile)
    {
        if (File.Exists(inputFile))
        {
            using (var fs = File.OpenRead(inputFile))
            {
                var inputStream = new AntlrInputStream(fs);
                var lexer = new QuackLexer(inputStream);
                var tokenStream = new CommonTokenStream(lexer);
                var parser = new QuackParser(tokenStream);
                var tree = parser.program();

                OtherErrors += parser.NumberOfSyntaxErrors;
                ast = (ProgramAstNode)new BuildAstVisitor().VisitProgram(tree);
            }

            // validate class hierarchy
            foreach (var c in ast.Classes)
            {
                // no need to report multiple circular dependencies
                if (!c.Value.ErrorDetected)
                {
                    ValidateClass(c.Value);
                }
            }

            // // now push methods to subclasses recursively
            // ast.Classes["Obj"].Methods = ast.Classes["Obj"].DeclaredMethods;
            // InheritStuff(ast.Classes["Obj"]);

            // we have our "valid" AST, now let's type check it
            var checker = new TypeChecker();

            // TypeCheck() returns a new ProgramAstNode that has been type checked
            DisplayProgram(ast, 3);
            Debug("------------------------------------", 3);
            checker.TypeCheck(ast);

            // now push methods to subclasses recursively
            ast.Classes["Obj"].Methods = ast.Classes["Obj"].DeclaredMethods;
            InheritStuff(ast.Classes["Obj"]);

            // checker.VerifyClassFields(ast);


            Debug("------------------------------------", 2);
            DisplayProgram(ast, 2);

            if (OtherErrors > 0)
            {
                Console.Error.WriteLine("{0}: {1} error{2}.", FileName, OtherErrors, (OtherErrors == 1 ? "" : "s"));
            }
            else
            {
                var generator = new CodeGenerator();
                generator.Generate(ast);
            }
        }
        else
        {
            ReportError(null, string.Format("File not found: '{0}'.", inputFile));
        }
    }
}
}
