using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AntlrTest
{
    class Program
    {
        private static void Main(string[] args)
        {
            if (args.Count() < 1)
            {
                Console.Error.WriteLine("Usage: scanner.exe <input file>");
            }
            else
            {
                // throw away any extra args
                LexString(args[0]);
            }
        }

        private static void LexString(string inputFile)
        {
            if (File.Exists(inputFile))
            {
                using (var fs = File.OpenRead(inputFile))
                {
                    var inputStream = new AntlrInputStream(fs);
                    var lexer = new QuackLexer(inputStream);

                    for (var token = lexer.NextToken(); token.Type != -1; token = lexer.NextToken())
                    {
                        Console.WriteLine("{0}  {1} \"{2}\"", token.Line, lexer.Vocabulary.GetSymbolicName(token.Type), token.Text);
                        if (lexer.Vocabulary.GetSymbolicName(token.Type).Equals("SQ_STR_BDE"))
                        {
                            Console.Error.WriteLine("{0}: Illegal escape code in quoted string ('{1}')",
                                     token.Line, token.Text);
                        }
                        else if (lexer.Vocabulary.GetSymbolicName(token.Type).Equals("SQ_STR_BDN"))
                        {
                            Console.Error.WriteLine("{0}: Encountered newline in quoted string ('{1}')",
                                     token.Line, token.Text);
                        }
                    }
                }
            } 
            else 
            {
                Console.Error.WriteLine("File not found: '{0}'", inputFile);
            }
        }
    }
}
